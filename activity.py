#input only integer
while True:
	try:
		year = int(input('Please input year\n'))
		break
	except ValueError:
		print("input integers please")
		continue

if (year>0 and year%4==0 and year%100!=0) or (year>0 and year%400==0):
	print(year, "is a leap year.")
else :
	print(year, "is not a leap year")

row = int(input('Please input rows\n'))
col = int(input('Please input columns\n'))
star="*"

for x in range(row):
	print (star*col+'')